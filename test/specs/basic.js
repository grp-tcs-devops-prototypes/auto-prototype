const { When } = require('cucumber');
const { Then } = require('cucumber');
When(
       /^I click on first CONTINUE$/,
       function() {
        const elem = $('//*[@id="root"]/div/div[2]/div/div/table/tbody/tr[1]/td[4]/a/button/span[1]')
        elem.click();
       }
    );
When(
    /^I click on the toggle button to see the quotes$/,
    function() {
     const elem = $('//*[@id="root"]/div/div[2]/div/h6[1]/div/span/span[1]/span[1]/input')
     elem.click();
    }
 );
When(
    /^I click on the "GET A NEW QUOTE" button to get a new quote$/,
    function() {
     const elem = $('//*[@id="root"]/div/div[2]/div/a/div/button/span[1]')
     elem.click();
    }
 );
 When(
    /^I click on the checkbox of the modal$/,
    function() {
     const elem = $('//*[@id="alert-dialog-slide-description"]/div/div[3]/span/span[1]/input')
     elem.click();
    }

 );
 When(
    /^I click on the continue button$/,
    function() {
     const elem = $('/html/body/div[3]/div[3]/div/div[3]/a/button/span[1]')
     elem.click();
    }
 );
Then(
        /^I should see "([^"]*)?" in the header$/,
        function(expectedTitle) {
        const title = $('h6*='+ expectedTitle +'').getText();
        expect(title).to
            .equal(
                expectedTitle,
                `Expected title to be "${expectedTitle}" but found "${title}"`
            );
        }
     );
Then(
        /^I should see "([^"]*)?" in the modal header$/,
        function(expectedTitle) {
        const title = $('//*[@id="alert-dialog-slide-title"]/h2').getText();
        expect(title).to
            .equal(
                expectedTitle,
                `Expected title to be "${expectedTitle}" but found "${title}"`
            );
        }
     );
Then(
        /^I should see "([^"]*)?" in the vehicles list$/,
        function(expectedTitle) {
        const title = $('//*[@id="root"]/div/div[2]/div/div[2]/div[2]/span[1]/div/div/div[2]/div/b').getText();
        expect(title).to
            .equal(
                expectedTitle,
                `Expected title to be "${expectedTitle}" but found "${title}"`
            );
        }
     );
     
Then(
        /^I should see coverage amount for "([^"]*)?" is "([^"]*)?"$/,
        function(expectedTitle, expectedAmount) {
            switch(expectedTitle) {
                case "Bodily Injury Liability Coverage":
                    const title1 = $('//*[@id="root"]/div/div[2]/div/div[2]/div[4]/div/div/div[1]/div/div[1]/div').getText();
                    expect(title1).to
                        .equal(
                            expectedTitle,
                                `Expected content to be "${expectedTitle}" but found "${title1}"`
                            );
                break;
                case "Property Damage":
                    const title2 = $('//*[@id="root"]/div/div[2]/div/div[2]/div[4]/div/div/div[2]/div/div[1]/div').getText();
                    expect(title2).to
                        .equal(
                                expectedTitle,
                                `Expected content to be "${expectedTitle}" but found "${title2}"`
                            );
                break;
                case "Comprehensive":
                    const title3 = $('//*[@id="root"]/div/div[2]/div/div[2]/div[4]/div/div/div[3]/div/div[1]/div').getText();
                    expect(title3).to
                        .equal(
                            expectedTitle,
                                `Expected content to be "${expectedTitle}" but found "${title3}"`
                            );
                break;
                case "Collision":
                    const title4 = $('//*[@id="root"]/div/div[2]/div/div[2]/div[4]/div/div/div[4]/div/div[1]/div').getText();
                    expect(title4).to
                        .equal(
                            expectedTitle,
                                `Expected content to be "${expectedTitle}" but found "${title4}"`
                            );
                break;   

                }    
            }
    );