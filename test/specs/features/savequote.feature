
Feature:
    In order to keep my product stable
    As a developer or product manager
    I want to make sure that everything works as expected as a general regression testing

Scenario: General Regression Testing of one saved quote
    Given I open the url "https://tcs-sat.netlify.com"
    Then I expect that the title is "Auto Insurance"
    Then I should see "Your Active Policy" in the header
    When I click on the toggle button to see the quotes
    # When I add "75605" to the inputfield "#zipcode"
    # When I add "Longview" to the inputfield "#city"
    # When I add "TEXAS" to the inputfield "#state" 
    Then I should see "Your Saved Quotes" in the header
    When I click on first CONTINUE
    Then  I expect that element "h6" contains the text "Auto Insurance Quote"
    # Then I should see "2018 Honda C" in the vehicles list
    # Then I should see coverage amount for "Bodily Injury Liability Coverage" is "$15,000/$30,000"
    # Then I should see coverage amount for "Property Damage" is "$10000"
    # Then I should see coverage amount for "Comprehensive" is "$500"
    # Then I should see coverage amount for "Collision" is "$500"
    # Then I should see "Click the below button to get started!" in the header
    # When I click on the "GET A NEW QUOTE" button to get a new quote
    # Then I should see "Start a family tradition of saving with Auto Insurance." in the modal header
    # When I click on the checkbox of the modal
    # When I click on the continue button