import React from "react";
import {shallow} from 'enzyme';
import {AddDriver} from '../../../src/Widgets/AddDriver/AddDriver';


describe ('Add Driver page', () => {

	test('render four input fields', () => {
		const wrapper = shallow(<AddDriver />);
		expect(wrapper.find('Input').length).toBe(4);
	}); 

});